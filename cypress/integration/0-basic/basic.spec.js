/// <reference types="cypress" />

describe('example Vue App', () => {
  it('calculates the product correctly', () => {
    cy.visit('http://localhost:8080')
    cy.get('[data-autotest-id="factor1"]').type('{backspace}1111', { delay: 100})
    cy.get('[data-autotest-id="factor2"]').type('{backspace}1111', { delay: 100})
    cy.get('span').should('include.text', 1234321)
  })
})
