# Cypress+GitLab Example

This repository shows how a [Cypress.io](https://www.cypress.io/) automated test can 
be used to test a Vue (or any other JavaScript) application as part of a GitLab 
pipeline. Results are shown as 📃 text and 🎞 video output.

![Screen recording](docs/recording.gif)

## Key parts of the project

### Vue app

The Vue app in [/src](/src) has been set up using the following command:

    vue create cypress-gitlab-example

### Cypress tests

Cypress has been added to the project using:

    yarn add cypress --save-dev

The tests are in [/cypress](/cypress).

The documentation used instructions from [here](https://docs.cypress.io/guides/getting-started/installing-cypress)

### GitLab pipeline

The [.gitlab-ci.yml](.gitlab-ci.yml) file has been written according to the specs from 
[here](https://docs.cypress.io/guides/continuous-integration/gitlab-ci#Basic-Setup).

## Process

When the stage "test" runs in GitLab, the following happens:

  * The Vue application is started using `yarn serve &`
  * The Cypress test runners is started in headless mode using `cypress run`
  * Screen recording is made available via build artifacts for download here:
    ![artifacts](docs/artifacts.png "Title Text")
  * Results are published to the Cypress.io Dashboard. (optional step)
  
## Personal experience

Before trying this super simple example, I tried enabling running Cypress on an app which is
using Nuxt and Firebase (services: Firestore and Auth)

  * Once, using a Firebase emulator within GitLab runners. 
  * And second, using the Firebase service at GCP.

In both cases I failed because of random timeouts while running tests 🤯. So I decided to try this
super-simple example to be successful.
